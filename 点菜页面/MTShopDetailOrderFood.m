//
//  MTShopDetailOrderFood.m
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import "MTShopDetailOrderFood.h"

@implementation MTShopDetailOrderFood

+ (instancetype)shopDetailOrderFood:(NSDictionary *)dict {
    MTShopDetailOrderFood *food = [[self alloc] init];
    [food setValuesForKeysWithDictionary:dict];
    return food;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"description"]) {
        _desc = value;
    }
}

@end
