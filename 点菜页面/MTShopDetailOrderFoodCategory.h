//
//  MTShopDetailOrderFoodCategory.h
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import "MTShopDetailOrderFood.h"
#import <Foundation/Foundation.h>

@interface MTShopDetailOrderFoodCategory : NSObject

/**
 * 分类名称
 */
@property (nonatomic, copy) NSString *name;

/**
 * 菜品模型的数组
 */
@property (nonatomic, strong) NSArray<MTShopDetailOrderFood *> *spus;

+ (instancetype)shopDetailOrderFoodCategory:(NSDictionary *)dict;

@end
