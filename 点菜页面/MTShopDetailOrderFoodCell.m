//
//  MTShopDetailOrderFoodCell.m
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import "MTShopDetailOrderFoodCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Addition.h"

@interface MTShopDetailOrderFoodCell ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLable;
@property (weak, nonatomic) IBOutlet UILabel *month_saled_contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *praise_contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *min_priceLabel;

@end

@implementation MTShopDetailOrderFoodCell

- (void)setFood:(MTShopDetailOrderFood *)food {
    _food = food;

    [self.pictureView sd_setImageWithURL:[NSURL URLWithString:[food.picture stringByDeletingPathExtension]] placeholderImage:[UIImage imageNamed:@"avatar"]];

    self.nameLabel.text = food.name;
    self.descLable.text = food.desc;
    self.month_saled_contentLabel.text = food.month_saled_content;
    self.praise_contentLabel.text = food.praise_content;
    self.min_priceLabel.text = [NSString stringWithFormat:@"￥%.1f", food.min_price];

    self.pictureView.contentMode = UIViewContentModeScaleAspectFill;
    self.pictureView.clipsToBounds = YES;
    self.pictureView.layer.cornerRadius = 4;
    self.pictureView.layer.masksToBounds =YES;
}

//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // ----- 视图 -----
//        // 头像
//        UIImageView *pictureView = [[UIImageView alloc] init];
//        pictureView.image = [UIImage imageNamed:@"avatar"];
//        [self.contentView addSubview:pictureView];
//
//        // 菜名字
//        UILabel *nameLabel = [UILabel labelWithText:@"testtesttesttesttesttesttesttesttesttesttesttesttest" andTextColor:[UIColor blackColor] andFontSize:14];
//        [self.contentView addSubview:nameLabel];
//
//        // 描述
//        UILabel *descLable = [UILabel labelWithText:@"miaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshumiaoshu" andTextColor:[UIColor darkGrayColor] andFontSize:12];
//        descLable.numberOfLines = 2;
//        [self.contentView addSubview:descLable];
//
//        // 月售
//        UILabel *month_saled_contentLabel = [UILabel labelWithText:@"yueshou" andTextColor:[UIColor darkGrayColor] andFontSize:12];
//        [self.contentView addSubview:month_saled_contentLabel];
//
//        // 赞
//        UILabel *praise_contentLabel = [UILabel labelWithText:@"zan" andTextColor:[UIColor darkGrayColor] andFontSize:12];
//        [self.contentView addSubview:praise_contentLabel];
//
//        // 价钱
//        UILabel *min_priceLabel = [UILabel labelWithText:@"yueshou" andTextColor:[UIColor darkGrayColor] andFontSize:12];
//        [self.contentView addSubview:min_priceLabel];
//
//        // ----- 布局 -----
//        // 头像布局
//        [pictureView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.left.offset(12);
//            make.width.height.offset(60);
//        }];
//
//        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(pictureView);
//            make.left.equalTo(pictureView).offset(12);
//        }];
//
//        [descLable mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(nameLabel.mas_bottom);
//            make.left.equalTo(nameLabel);
//            make.right.offset(-12);    // 两行
//        }];
//
//        [month_saled_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(descLable.mas_bottom);
//            make.left.equalTo(descLable);
//        }];
//
//        [praise_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(month_saled_contentLabel);
//            make.left.equalTo(month_saled_contentLabel.mas_right).offset(12);
//        }];
//
//        [min_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(month_saled_contentLabel.mas_bottom).offset(20);
//            make.left.equalTo(month_saled_contentLabel);
//            make.bottom.offset(-10);
//        }];
//    }
//
//    return self;
//}
@end
