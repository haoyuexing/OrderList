//
//  MTShopDetailOrderFoodCell.h
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import "MTShopDetailOrderFood.h"
#import <UIKit/UIKit.h>

@interface MTShopDetailOrderFoodCell : UITableViewCell

@property (strong, nonatomic) MTShopDetailOrderFood *food;

@end
