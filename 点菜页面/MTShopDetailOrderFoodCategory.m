//
//  MTShopDetailOrderFoodCategory.m
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import "MTShopDetailOrderFood.h"
#import "MTShopDetailOrderFoodCategory.h"

@implementation MTShopDetailOrderFoodCategory

+ (instancetype)shopDetailOrderFoodCategory:(NSDictionary *)dict {
    MTShopDetailOrderFoodCategory *foodCategory = [[self alloc] init];
    [foodCategory setValuesForKeysWithDictionary:dict];
    return foodCategory;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"spus"]) {
        // 字典转模型
        NSArray *tempArray = value;

        NSMutableArray *array = [NSMutableArray array];

        // 字典转模型
        for (NSDictionary *dict in tempArray) {
            MTShopDetailOrderFood *f = [MTShopDetailOrderFood shopDetailOrderFood:dict];
            [array addObject:f];
        }

        _spus = array.copy;
        return;
    }

    [super setValue:value forKey:key];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
}

@end
