//
//  MTShopDetailOrderFoodCategoryCell.m
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import "MTShopDetailOrderFoodCategoryCell.h"
#import "Masonry.h"
#import "UIColor+Addition.h"

@interface MTShopDetailOrderFoodCategoryCellBGView : UIView

@end

@implementation MTShopDetailOrderFoodCategoryCellBGView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        // 黄色视图
        UIView *yellowView = [[UIView alloc] init];
        yellowView.backgroundColor = [UIColor primaryYellowColor];
        [self addSubview:yellowView];

        [yellowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.top.offset(10);
            make.bottom.offset(-10);
            make.width.offset(4);
        }];
    }
    return self;
}

@end

@implementation MTShopDetailOrderFoodCategoryCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 背景view
        MTShopDetailOrderFoodCategoryCellBGView *bgView = [[MTShopDetailOrderFoodCategoryCellBGView alloc] init];
        self.selectedBackgroundView = bgView;

        self.contentView.backgroundColor = [UIColor colorWithHex:0xF8F8F8];
    }
    return self;
}

@end
