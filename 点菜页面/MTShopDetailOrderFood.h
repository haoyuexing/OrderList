//
//  MTShopDetailOrderFood.h
//  点菜页面
//
//  Created by HaoYoson on 16/7/22.
//  Copyright © 2016年 HaoYoson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTShopDetailOrderFood : NSObject

/**
 * 菜品名称
 */
@property (nonatomic, copy) NSString *name;
/**
 * 月售
 */
@property (nonatomic, copy) NSString *month_saled_content;
/**
 * 点赞数
 */
@property (nonatomic, copy) NSString *praise_content;
/**
 * 售价
 */
@property (nonatomic, assign) CGFloat min_price;
/**
 * 描述
 */
@property (nonatomic, copy) NSString *desc;
/**
 * 配图 URL 字符串
 */
@property (nonatomic, copy) NSString *picture;

+ (instancetype)shopDetailOrderFood:(NSDictionary *)dict;

@end
